#include "data_structure.h"
#include <string>
#include <iostream>


data_structure::data_structure() {
    // Default constructor: Generate an empty data structure
    head=nullptr; //singly linked list.
}

data_structure::data_structure(std::string input_string) {
    // String constructor: Construct a data structure and store the input string into it

        node *n = new node(input_string);
        for(int i=0; i<input_string.length(); i++) {
            n->value = input_string[i] - '0';
        }


}
void data_structure::insert_str(std::string input_string)
{
    node *curr=head;
    for(int i=0; i<input_string.length(); i++)
    {
        node *n = new node(input_string);
        n->value=input_string[i] - '0'; // convert using ascii value
        n->next= nullptr;
        head=n;
    }

}

data_structure::~data_structure() {
    // Default Deconstructor: Deconstruct the data structure
    if(head==nullptr)
    {
        return;
    }
    else
    {
        node*curr=head;
        while(curr!= nullptr)
        {
            if(curr->next== nullptr)
            {
                head= nullptr;
                delete curr;
            } else{
                node *temp=curr->next;
                head=temp;
                delete curr;
                curr=temp;
            }
        }
    }
}

unsigned int data_structure::frequency(char input_character) {
    // Given a character, return the frequency of that character in the data structure.
     int c = (input_character - '0'); //convert char to  int
    node *curr=head;
    int count;
    while(curr!= nullptr)
    {
        if(curr->value == c) //compare stored int value with converted int value
        {
            count++; // if equal, increment counter;
        }
        curr=curr->next;
    }
    return count;
}

char data_structure::most_frequent() {
    // Return the most frequent character in the data structure
    return 0;
}

char data_structure::least_frequent() {
    // Return the least frequent character in the data structure that actually exists
    return 0;
}

void data_structure::sort() {
    // Sort the data structure first by frequency, greatest to least and then by character value, least to greatest.
    // Example: a:3,z:4,c:3,d:1,v:1,e:2         sorted: z:4,a:3,c:3,e:2,d:1,v:1
}
void data_structure::print()
{
    node*curr=head;
    while(curr!= nullptr)
    {
        std::cout<<curr->value<<std::endl;
        curr=curr->next;
    }

}

std::istream &operator>>(std::istream &stream, data_structure &structure) {
    // Stream in a string, deconstruct the current data structure and create a new data structure with the new streamed
    // in string.
    return stream;
}

std::ostream &operator<<(std::ostream &stream, const data_structure &structure) {
    // Stream out the data structure
    // Output in this format "<character>:<frequency>,<character>:<frequency>,<character>:<frequency>"
    return stream;
}
